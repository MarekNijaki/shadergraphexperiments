﻿using System.Linq;
using UnityEngine;

public class FlipNormals : MonoBehaviour
{
    [ContextMenu("FlipNormals")]
    public void FlipNormalsOnObject()
    {
        Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
        mesh.triangles = mesh.triangles.Reverse().ToArray();
    }
}
