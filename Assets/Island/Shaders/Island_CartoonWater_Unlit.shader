Shader "Custom/CartoonWater"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Opacity ("Opacity", Range(0,1)) = 0.5
        _AnimSpeedX ("Anim Speed (X)", Range(0,4)) = 1.3
        _AnimSpeedY ("Anim Speed (Y)", Range(0,4)) = 2.7
        _AnimScale ("Anim Scale", Range(0,1)) = 0.03
        _AnimTiling ("Anim Tiling", Range(0,20)) = 8
    }
    SubShader
    {
        // Set water as transparent material.
        Tags { "RenderType"="Transparent" "Queue"="Transparent" }
        LOD 100
        // Water won’t obscure other objects in the depth buffer.
        ZWrite Off
        // Set blend to Alpha blend.
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            // Include Unity CG library (defines with methods).
            #include "UnityCG.cginc"

            struct InputData
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float4 vcolor : COLOR;
            };

            struct Interpolators
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            // Define variables to use properties data.
            sampler2D _MainTex;
            float4 _MainTex_ST;
            half _Opacity;
            float _AnimSpeedX;
            float _AnimSpeedY;
            float _AnimScale;
            float _AnimTiling;

            // Vertex shader.
            Interpolators vert (InputData inputData)
            {
                Interpolators interpolators;
                interpolators.vertex = UnityObjectToClipPos(inputData.vertex);
                interpolators.uv = TRANSFORM_TEX(inputData.uv, _MainTex);
                // Return value.
                return interpolators;
            }

            // Fragments (pixel) shader.
            fixed4 frag (Interpolators interpolators) : SV_Target
            {
                // Both lines of code do essentially the same thing but in different directions.
                interpolators.uv.x += sin((interpolators.uv.x + interpolators.uv.y) * _AnimTiling + _Time.y * _AnimSpeedX) * _AnimScale;
                interpolators.uv.y += cos((interpolators.uv.x - interpolators.uv.y) * _AnimTiling + _Time.y * _AnimSpeedY) * _AnimScale;
                // Sample the texture.
                fixed4 col = tex2D(_MainTex, interpolators.uv);
                // Set opacity.
                col.a = _Opacity;
                // Return value.
                return col;
            }
            ENDCG
        }
    }
}
