using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(Collider))]
public class GameObjectHighlighter : MonoBehaviour
{
    /// <summary>
    ///   Original material of game object.
    /// </summary>
    [SerializeField]
    [Tooltip("Original material of game object")]
    private Material _originalMaterial;

    /// <summary>
    ///   Material used to highlight game object.
    /// </summary>
    [SerializeField]
    [Tooltip("Material used to highlight game object")]
    private Material _highlightMaterial;
    
    /// <summary>
    ///   Mesh renderer.
    /// </summary>
    private MeshRenderer _meshRenderer;

    /// <summary>
    ///   Start.
    /// </summary>
    private void Start()
    {
        // Cache a reference to the MeshRenderer.
        _meshRenderer = GetComponent<MeshRenderer>();
        // Set default material at the beginning.
        EnableHighlight(false);
    }
    
    /// <summary>
    ///   On mouse over (requires MeshCollider).
    /// </summary>
    private void OnMouseOver()
    {
        EnableHighlight(true);
    }

    /// <summary>
    ///   On mouse exit (requires MeshCollider).
    /// </summary>
    private void OnMouseExit()
    {
        EnableHighlight(false);
    }

    /// <summary>
    ///   Toggle between the original and highlighted materials.
    /// </summary>
    /// <param name="enableHighlight">Flag if highlight should be enabled</param>
    private void EnableHighlight(bool enableHighlight)
    {
        // If variables are not filled then exit.
        if((_meshRenderer == null) || (_originalMaterial == null) || (_highlightMaterial == null))
        {
            return;
        }
        // Set material depending on highlight state.
        _meshRenderer.material = enableHighlight ? _highlightMaterial : _originalMaterial;
    }
}