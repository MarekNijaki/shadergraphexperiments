﻿/*
 * Copyright (c) 2019 Razeware LLC
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Notwithstanding the foregoing, you may not use, copy, modify, merge, publish, 
 * distribute, sublicense, create a derivative work, and/or sell copies of the 
 * Software in any work that is designed, intended, or marketed for pedagogical or 
 * instructional purposes related to programming, coding, application development, 
 * or information technology.  Permission for such use, copying, modification,
 * merger, publication, distribution, sublicensing, creation of derivative works, 
 * or sale is expressly withheld.
 *    
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using System;
using UnityEngine;

// Control one of the seven tangram game pieces.
public class GamePiece : MonoBehaviour
{
    /// <summary>
    ///   Rotation degrees per second.
    /// </summary>
    [SerializeField]
    [Tooltip("Rotation degrees per second")]
    private float _rotationSpeed = 90.0F;
    
    /// <summary>
    ///   Camera used on the scene.
    /// </summary>
    private Camera _camera;
    
    /// <summary>
    ///   Mouse world position when drag started.
    /// </summary>
    private Vector3 _mouseDragStartWorldPosition;
    
    /// <summary>
    ///   Game object world position when drag started.
    /// </summary>
    private Vector3 _gameObjectDragStartWorldPosition;

    /// <summary>
    ///   Distance from camera to clicked object.
    /// </summary>
    private float _distanceFromCameraToClickedObject;

    /// <summary>
    ///   Flag informing if mouse is over game piece.
    /// </summary>
    private bool _isMouseOverGamePiece;

    /// <summary>
    ///   Desired/target rotation of game piece.
    /// </summary>
    private Quaternion _targetRotation;

    /// <summary>
    ///   Flag informing if game piece is currently rotating.
    /// </summary>
    private bool _isRotating;

    /// <summary>
    ///   Game object default 'y' value.
    /// </summary>
    private const float _defaultYValue = 0.0F;

    /// <summary>
    ///   Awake.
    /// </summary>
    private void Awake()
    {
        _camera = Camera.main;
    }

    /// <summary>
    ///   On mouse down.
    /// </summary>
    private void OnMouseDown()
    {
        // Get object position.
        _gameObjectDragStartWorldPosition = gameObject.transform.position;
        // Calculate screen 'z' value for this GameObject.
        //   WorldToScreenPoint():
        //     Transforms position from world space into screen space.
        //     Screenspace is defined in pixels.
        //     The bottom-left of the screen is (0,0); the right-top is (pixelWidth,pixelHeight).
        //     The 'z' position is in world units from the camera.
        _distanceFromCameraToClickedObject = _camera.WorldToScreenPoint(_gameObjectDragStartWorldPosition).z;
        // Get position of mouse when button is pressed for the first time.
        _mouseDragStartWorldPosition = MouseToWorldPoint();
    }

    /// <summary>
    ///   Return the mouse position as a world space position.
    /// </summary>
    /// <returns>Mouse world space position</returns>
    private Vector3 MouseToWorldPoint()
    {
        // Get mouse screen position.
        Vector3 mouseScreenPoint = Input.mousePosition;
        // Set depth to the distance from object.
        mouseScreenPoint.z = _distanceFromCameraToClickedObject;
        // Convert to world space.
        //   ScreenToWorldPoint()
        //     Return the world space point created by converting the screen space point
        //     at the provided distance of 'z' subcomponent, from the camera plane.
        return _camera.ScreenToWorldPoint(mouseScreenPoint);
    }

    /// <summary>
    ///   Drag the GameObject along the 'xz' plane (requires MeshCollider).
    /// </summary>
    private void OnMouseDrag()
    {
        // Get mouse drag end world position.
        Vector3 mouseDragEndWorldPosition = MouseToWorldPoint();
        // Calculate drag distance.
        Vector3 dragDistance = _mouseDragStartWorldPosition - mouseDragEndWorldPosition;
        // Move object.
        transform.position = _gameObjectDragStartWorldPosition - dragDistance;
        // Keep game pieces on the ground. 
        transform.position = new Vector3(transform.position.x, _defaultYValue, transform.position.z);
    }
    
    /// <summary>
    ///   Apply rotation to game object.
    /// </summary>
    private void ApplyRotation()
    {
        if(!_isRotating)
        {
            return;
        }
        
        transform.rotation = Quaternion.RotateTowards(transform.rotation, _targetRotation, _rotationSpeed * Time.deltaTime);

        // If game piece is close to target rotation, then complete the rotation.
        if(Mathf.Abs(Quaternion.Angle(_targetRotation, transform.rotation)) < 1.0f)
        {
            transform.rotation = _targetRotation;
            _isRotating = false;
        }
    }

    /// <summary>
    ///   On mouse enter (requires MeshCollider).
    /// </summary>
    public void OnMouseEnter()
    {
        _isMouseOverGamePiece = true;
    }

    /// <summary>
    ///   On mouse exit (requires MeshCollider).
    /// </summary>
    public void OnMouseExit()
    {
        _isMouseOverGamePiece = false;
    }

    /// <summary>
    ///   Update.
    /// </summary>
    private void Update()
    {
        // Apply rotation.
        ApplyRotation();
        
        // If mouse is not over piece or piece is still rotating then exit.
        if(!_isMouseOverGamePiece || _isRotating)
        {
            return;
        }

        // Set target rotation vector (angle).
        SetTargetRotationVector();
    }

    /// <summary>
    ///   Set target rotation vector (angle).
    /// </summary>
    private void SetTargetRotationVector()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            RotateCounterClockwise();
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            RotateCounterClockwise();
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            RotateClockwise();
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            RotateClockwise();
        }
    }
    
    /// <summary>
    ///   Set a target rotation of 15 degrees counter clockwise.
    /// </summary>
    private void RotateClockwise()
    {
        _isRotating = true;

        float newY = Mathf.RoundToInt(transform.rotation.eulerAngles.y + 15.0F);
        _targetRotation = Quaternion.Euler(0f, newY, 0f);
    }

    /// <summary>
    ///   Set a target rotation of 15 degrees clockwise.
    /// </summary>
    private void RotateCounterClockwise()
    {
        _isRotating = true;

        float newY = Mathf.RoundToInt(transform.rotation.eulerAngles.y - 15.0F);
        _targetRotation = Quaternion.Euler(0f, newY, 0f);
    }
}
